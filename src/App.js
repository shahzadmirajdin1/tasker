import { BrowserRouter as Router, Navigate } from "react-router-dom";
import { Routes, Route } from "react-router-dom";
import BulkDelete from "./pages/bulkDelete/BulkDelete";
import CreateTask from "./pages/createTask/CreateTask";
import ListTasks from "./pages/listTasks/ListTasks";
import { Provider } from "react-redux";
import store from "./redux/store";
import "./app.css";
import Header from "./components/header/Header";
import { Container } from "@mui/material";
import { makeStyles } from "@mui/styles";
const useStyles = makeStyles({
	root: {
		// textAlign: "center",
		"& nav": {
			marginBottom: "20px ",
		},
	},
});
function App() {
	const classes = useStyles();
	return (
		<div className="App">
			<Provider store={store}>
				<Container maxWidth="md" className={classes.root}>
					<Router>
						<Header />
						<Routes>
							<Route path="/list-tasks" element={<ListTasks />} />
							<Route path="/create-task" element={<CreateTask />} />
							<Route path="/bulk-delete" element={<BulkDelete />} />
							<Route path="/" element={<Navigate replace to="/list-tasks" />} />
						</Routes>
					</Router>
				</Container>
			</Provider>
		</div>
	);
}

export default App;
