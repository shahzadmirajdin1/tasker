import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Card from "./../../components/card/Card";
import { nanoid } from "nanoid";

export default function ListTasks() {
	const { listItem: inialListItem } = useSelector((state) => state.tasker);
	const [listItem, setListItem] = useState([]);
	useEffect(() => {
		setListItem(inialListItem);
	}, [inialListItem]);
	return (
		<div>
			{listItem?.length > 0 &&
				listItem?.map((item, index) => (
					<Card {...{ item, index }} key={nanoid()} />
				))}
			{listItem?.length === 0 && <h3>Record not found...</h3>}
		</div>
	);
}
