import React, { useState } from "react";
import { TextField, IconButton } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useDispatch } from "react-redux";
import { nanoid } from "nanoid";
import { AddListItemAction } from "../../redux/taskerReducer.js";
import { useNavigate } from "react-router-dom";
export default function CreateTask() {
  const [value, setValue] = useState("");
  const dispatch = useDispatch();
  let navigate = useNavigate();
  const id = nanoid();
  const handleChange = (event) => {
    setValue(event.target.value);
  };
  const handleSave = () => {
    if (value.trim() != "") {
      dispatch(AddListItemAction({ id, title: value }));
      navigate("/", { replace: true });
    }
  };
  return (
    <TextField
      label="Add item to list"
      value={value}
      onChange={handleChange}
      InputProps={{
        endAdornment: (
          <IconButton onClick={handleSave}>
            <AddIcon />
          </IconButton>
        ),
      }}
    />
  );
}
