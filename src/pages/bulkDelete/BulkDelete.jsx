import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { nanoid } from "nanoid";
import { IconButton, FormControlLabel, Checkbox } from "@mui/material";
import ClearIcon from "@mui/icons-material/Clear";
import Card from "./../../components/card/Card";
import {
	bulkDeleteItemsAction,
	deleteListItemAction,
} from "../../redux/taskerReducer";
export default function BulkDelete() {
	const { listItem: inialListItem } = useSelector((state) => state.tasker);
	const [listItem, setListItem] = useState([]);
	useEffect(() => {
		setListItem(inialListItem);
	}, [inialListItem]);

	const dispatch = useDispatch();
	const handleCheckbox = (id) => {
		const temp = [...listItem];
		const item = temp?.map((item) => {
			if (item.id === id) {
				return { ...item, checked: !item?.checked };
			}
			return item;
		});
		setListItem(item);
	};
	const selectAllHandler = (e) => {
		let isSelected = e.target.checked;
		const temp = [...listItem];
		const item = temp?.map((item) => {
			return { ...item, checked: isSelected };
		});
		setListItem(item);
	};
	const bulkDeleteHandler = () => {
		const temp = [...listItem];
		dispatch(bulkDeleteItemsAction(temp));
	};
	const handleSingleDelete = (id) => {
		dispatch(deleteListItemAction(id));
	};
	return (
		<div>
			<FormControlLabel
				value="selectAll"
				control={<Checkbox onChange={selectAllHandler} />}
				label="Select All"
				labelPlacement="end"
			/>
			<IconButton onClick={bulkDeleteHandler}>
				<ClearIcon color="error" />
			</IconButton>
			{listItem?.length > 0 &&
				listItem?.map((item) => (
					<Card
						{...{ item, handleCheckbox, handleSingleDelete }}
						key={nanoid()}
					/>
				))}
			{listItem?.length === 0 && <h3>Record not found...</h3>}
		</div>
	);
}
