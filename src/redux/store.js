import { configureStore } from "@reduxjs/toolkit";
import taskerReducer from "./taskerReducer";

export default configureStore({
	reducer: { tasker: taskerReducer },
});
