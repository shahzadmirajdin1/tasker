import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	listItem: [],
};

const taskerSlicer = createSlice({
	name: "taskerReducer",
	initialState,
	reducers: {
		AddListItemAction: (state, action) => {
			state.listItem.push(action.payload);
		},
		deleteListItemAction: (state, action) => {
			let id = action.payload;
			state.listItem?.splice(
				state.listItem?.findIndex((item) => item.id === id),
				1
			);
		},
		bulkDeleteItemsAction: (state, action) => {
			let items = action.payload;
			let myArray = state.listItem?.filter(
				(ar) => !items?.find((rm) => rm.id === ar.id && rm.checked === true)
			);
			state.listItem = myArray;
		},
	},
	extraReducers: {},
});

const taskerReducer = taskerSlicer.reducer;
export const {
	AddListItemAction,
	deleteListItemAction,
	bulkDeleteItemsAction,
} = taskerSlicer.actions;
export default taskerReducer;
