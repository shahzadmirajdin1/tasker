import React from "react";
import { makeStyles } from "@mui/styles";
import { IconButton, Box, FormControlLabel, Checkbox } from "@mui/material";
import ClearIcon from "@mui/icons-material/Clear";
const useStyles = makeStyles({
	card: {
		"& ul": {
			display: "flex",
			gap: "11px",
		},
		"& ul li": {
			display: "inline",
		},
	},
});
function Card({ item, handleCheckbox, handleSingleDelete, index }) {
	const { id, title, checked } = item;
	const classes = useStyles();
	return (
		<Box className={classes.card} component="h2">
			{handleCheckbox ? (
				<>
					<FormControlLabel
						value={id}
						control={
							<Checkbox
								checked={checked}
								onChange={(e) => handleCheckbox(id)}
							/>
						}
						label={title}
						labelPlacement="end"
					/>
					<IconButton onClick={(e) => handleSingleDelete(id)}>
						<ClearIcon />
					</IconButton>
				</>
			) : (
				<ul>
					<li>{index + 1}</li>
					<li>{title}</li>
				</ul>
			)}
		</Box>
	);
}

export default Card;
