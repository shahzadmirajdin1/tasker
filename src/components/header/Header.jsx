import React from "react";
import { NavLink } from "react-router-dom";
const routes = [
	{
		to: "list-tasks",
		title: "List Tasks",
	},
	{
		to: "create-task",
		title: "Create Task",
	},
	{
		to: "bulk-delete",
		title: "Bulk Delete",
	},
];
export default function Header() {
	return (
		<nav>
			<ul>
				{routes?.map((item) => (
					<li key={item.title}>
						<NavLink
							to={item?.to}
							className={({ isActive }) => (isActive ? "active" : undefined)}
						>
							{item?.title}
						</NavLink>
					</li>
				))}
			</ul>
		</nav>
	);
}
